const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const {check, validationResult} = require('express-validator');

const Lesson = require('../models/Lesson');

// @route     POST api/lesson
// @desc      Add new lesson
// @access    Private
router.post('/',
  [auth,
    [
      check('name', 'Name is required')
        .not()
        .isEmpty(),
      check('keySet', 'Key set is required')
        .not()
        .isEmpty(),
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({errors: errors.array()});
    }

    const {name, keySet, type} = req.body;
    try {
      const newLesson = new Lesson({
        name,
        keySet,
        type,
        by: req.user.id
      });

      const lesson = await newLesson.save();

      res.json(lesson);
    }catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

// @route     GET api/lesson
// @desc      Get lesson by id
// @access    Public
router.get('/:id', async (req, res) => {
  try {
    const lesson = await Lesson.findById(req.params.id);
    res.json({data: lesson});
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

module.exports = router;