const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const {check, validationResult} = require('express-validator');

const User = require('../models/User');
const Comment = require('../models/Comment');

// @route     GET api/comments
// @desc      Get all comments
// @access    Public
router.get('/', async (req, res) => {
  try {
    const comments = await Comment.find().sort({date: -1});
    res.json({data: comments});
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

// @route     POST api/comments
// @desc      Add new comment
// @access    Private
router.post('/',
  [auth,
    [
      check('text', 'Text is required')
        .not()
        .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({errors: errors.array()});
    }

    const {text} = req.body;
    try {
      const newComment = new Comment({
        text,
        by: req.user.id
      });

      const comment = await newComment.save();

      res.json({data: comment});
    }catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

// @route     PUT api/comments/:id
// @desc      Update comment
// @access    Private
router.put('/:id', auth, async (req, res) => {
  const {text} = req.body;

  // Build comment object
  const commentFields = {};
  if (text) commentFields.text = text;

  try {
    let comment = await Comment.findById(req.params.id);

    if (!comment) return res.status(404).json({msg: 'Comment not found'});

    // Make sure user owns comment
    if (comment.user.toString() !== req.user.id) {
      return res.status(401).json({msg: 'Not authorized'});
    }

    comment = await Comment.findByIdAndUpdate(
      req.params.id,
      {$set: commentFields},
      {new: true},
    );

    res.json({data: comment});
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

// @route     DELETE api/comments/:id
// @desc      Delete comment
// @access    Private
router.delete('/:id', auth, async (req, res) => {
  try {
    let comment = await Comment.findById(req.params.id);

    if (!comment) return res.status(404).json({msg: 'Comment not found'});

    // Make sure user owns comment
    if (comment.user.toString() !== req.user.id) {
      return res.status(401).json({msg: 'Not authorized'});
    }

    await Comment.findByIdAndRemove(req.params.id);

    res.json({msg: 'Comment removed'});
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

module.exports = router;
