const mongoose = require('mongoose');

const LessonSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
    default: 'Lesson',
  },
  by: {
    type:  mongoose.Schema.Types.ObjectId,
    ref: 'user',
  },
  keySet: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    default: 'basic',
  },
});

module.exports = mongoose.model('lesson', LessonSchema);