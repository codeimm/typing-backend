const express = require('express');
const router = express.Router();

const Lesson = require('../models/Lesson');

// @route     GET api/lessons
// @desc      Get all lessons
// @access    Public
router.get('/', async (req, res) => {
  try {
    const lessons = await Lesson.find({}).sort({id: 1});
    res.json({data: lessons});
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

module.exports = router;